#pragma once
/* version 1.0 of my school*/

#include <iostream>
#include <string>

#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using namespace std;

class Course
{
public:
	//Methods
	void init(string name, int test1, int test2, int exam);

	int* getGradesList();
	string getName();
	double getFinalGrade();


private:
	string _name;
	int _grades[3]; //array of size 3 => test1, test2, exam
	double _avg;
};
