#pragma once
/* version 1.0 of my school*/

#include "Course.h"

class Student
{
public:
	void init(string name, Course** courses, int crsCount);

	string getName();
	void setName(string name);
	int getCrsCount();
	Course** getCourses();
	double getAvg();


private:
	string _name;
	Course** _Courses;//array of pointers to Course
	int _crsCount;
};